cd ~/Documents/Auto-OBS-overlay/
Git pull
open --background --hide -a "parsec"
pkill -9 OBS
open -a "OBS" --args "--disable-shutdown-check"
osascript -e 'tell app "Terminal" to do script "npx @kqhivemind/hivemind-client ~/Documents/config.json"'

