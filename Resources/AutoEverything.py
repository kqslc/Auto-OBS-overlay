'''
1) copy out device info - done
2) override the scene files 
3) put device info back in
4) replace the cab info with the real cab info - done
5) start OBS - done
'''


import json
import os
from subprocess import call
import platform
import shutil
import glob

# Check the operating system
os_name = platform.system()

if os_name == 'Darwin':  # macOS
    OBS_scene = os.path.expanduser("~") + '/Library/Application Support/obs-studio/basic/scenes'
elif os_name == 'Windows': # windows
    file_path = os.path.join('C:', 'Path to OBS', 'AutoEverything.json')
else: # Linux
    file_path = os.path.join('default_directory', 'AutoEverything.json')


CabInfo_path = '../CabInfo.json'
SceneInfo_path = '../SceneInfo.json'

# Read the OBS JSON file
with open(os.path.join(OBS_scene,"AutoEverything.json"), 'r') as file:
    OBS_data = json.load(file)

with open(CabInfo_path, 'r') as file:
    CabInfo_data = json.load(file)

with open(SceneInfo_path, 'r') as file:
    SceneInfo_data = json.load(file)

devices = []
# we are going to fix the hivemind URLs
def FixURLs():
    # Extract the URL for each item in sources
    urls = []
    for item in OBS_data['sources']:
        if 'settings' in item and 'url' in item['settings']:
            print(item['settings']['url'])
            urls.append(item['settings']['url'])

    # Print the URLs
    for url in urls:
        print(url.replace("49", CabInfo_data['Cab ID']))

def CopySceneFiles():
    shutil.rmtree(os.path.join("..","Scene-collection","Live"))
    os.remove(os.path.join(OBS_scene,"AutoEverything.json"))
    shutil.copytree(os.path.join("..","Scene-collection",SceneInfo_data['Assets folder']),os.path.join("..","Scene-collection","Live"))

    # Find all JSON files in the source directory
    json_files = glob.glob(os.path.join(os.path.join("..","Scene-collection","Live"), '*.json'))

    # Copy each JSON file to the destination directory
    for file_path in json_files:
        shutil.copy(file_path,OBS_scene)

def GetDevices():
    global devices
    # Extract the Devices for each item in sources
    for item in OBS_data['sources']:
        if 'settings' in item and 'device' in item['settings']:
            devices += item['settings']
    
    print("We found existing camera data: " + str(devices))

def SetDevices():
    global devices
    deviceCount = 0
    
    for item in OBS_data['sources']:
        if 'settings' in item and 'device' in item['settings']:
            item['settings']['device'] = devices[deviceCount]
            deviceCount += 1
            print("Fixing cameras: " + item['settings']['device_name'])

    with open(OBS_scene, 'w') as file:
        json.dump(OBS_data, file)

def StartOBS():
    OBS_flags = ["open", "-a", "OBS", "--args", "--disable-shutdown-check"]
    OBS_flags += ["--collection", SceneInfo_data['OBS collection']]
    OBS_flags += ["--scene", SceneInfo_data['OBS scene']]
    if SceneInfo_data["Start Recording"]:
        OBS_flags += ["--startrecording"]
    if SceneInfo_data["Startreplaybuffer"]:
        OBS_flags += ["--startreplaybuffer"]
    if SceneInfo_data["Start Stream"]:
        OBS_flags += ["--startstreaming"]

    call(["pkill", "-9", "OBS"])
    call(OBS_flags)

#GetDevices()
CopySceneFiles()
#SetDevices()
FixURLs()
StartOBS()