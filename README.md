# Auto-OBS-overlay
This project is still a work in progress, a X will be next to each thing that is working, and a red X next to everything that is still being worked on, see the issue tracker for more information.

## Getting started to edit
❎ You should be able to simply import from whatever folder you want in the Scene-collection folder and edit away, then when you are done, export that into the same folder. Make sure to commit and push those changes and ❌ then when you re-boot the streaming computer (probably via parsec) it will pull those new changes in, make the edit for that computer automatically, and be ready to go.

## Getting started on new StreamingSystem

 - Download [python](https://www.python.org/)
 - Download [Parsec](https://parsec.app/downloads)
 - Download [OBS](https://obsproject.com/download)

### Mac
 - Add the StartMax.sh to your startup apps [Link](https://support.apple.com/guide/mac-help/open-items-automatically-when-you-log-in-mh15189/)
 - Setup auto login [Link](https://support.apple.com/en-us/102316)

### Windows
 - Add the StartWin.bat file to your startup items
 - Setup auto login [Link](https://www.ionos.com/digitalguide/server/configuration/windows-10-automatic-login/) on windows it is a bit more diffucult so be careful


### linux
 - Add the StartupLin.sh file to your startup items
 - Setup auto login [Link](https://help.ubuntu.com/stable/ubuntu-help/user-autologin.html.en)

 ## Setting up configurations
 ❎ Every machine will need a CabInfo.json file that you will need in the root of the repo. This will hold information like what scene collection to use, what camera sources are used where, and if you want to auto start streaming/recording.